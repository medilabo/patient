package com.openclassrooms.medilabo.patient.IT.controller;

import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class GetPatientIT {

    @Autowired
    private MockMvc mockMvc;

    private MockHttpServletRequestBuilder getRequest(Long patinentId) {
        return get("/patients/"+patinentId);
    }

    @Test
    void shouldReturn401WhenUnAuthenticated() throws Exception {
        // Given
        // When
        mockMvc.perform(getRequest(1L))
                .andExpect(status().isUnauthorized());
        // Then
    }

    @Test
    @WithMockUser(username = "user1", password = "password1")
    void shouldReturnPatient() throws Exception {
        // Given
        // When
        MvcResult mvcResult = mockMvc.perform(getRequest(1L))
                .andExpect(status().isOk())
                .andReturn();
        // Then
        JSONAssert.assertEquals("""
                {
                   "id":1,
                   "firstName":"TestNone",
                   "lastName":"Test",
                   "birthday":"1966-12-31",
                   "gender":"F",
                   "address":"1 Brookside St",
                   "phoneNumber":"100-222-3333"
                 }
                """, mvcResult.getResponse().getContentAsString(), true);
    }
}

package com.openclassrooms.medilabo.patient.IT.controller;

import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class GetPatientsIT {

    @Autowired
    private MockMvc mockMvc;

    private MockHttpServletRequestBuilder getRequest(Integer page, Integer size) {
        return get("/patients")
                .param("page", String.valueOf(page))
                .param("size", String.valueOf(size));
    }

    @Test
    void shouldReturn401WhenUnAuthenticated() throws Exception {
        // Given
        // When
        mockMvc.perform(getRequest(null, null))
                .andExpect(status().isUnauthorized());
        // Then
    }

    @Test
    @WithMockUser(username = "user1", password = "password1")
    void shouldReturnAllPatients() throws Exception {
        // Given
        // When
        MvcResult mvcResult = mockMvc.perform(getRequest(null, null))
                .andExpect(status().isOk())
                .andReturn();
        // Then
        JSONAssert.assertEquals("""
                {
                  "content":[
                    {
                      "id":1,
                      "firstName":"TestNone",
                      "lastName":"Test",
                      "birthday":"1966-12-31",
                      "gender":"F",
                      "address":"1 Brookside St",
                      "phoneNumber":"100-222-3333"
                    },
                    {
                      "id":2,
                      "firstName":"TestBorderline",
                      "lastName":"Test",
                      "birthday":"1945-06-24",
                      "gender":"M",
                      "address":"2 High St",
                      "phoneNumber":"200-333-4444"
                    },
                    {
                      "id":3,
                      "firstName":"TestInDanger",
                      "lastName":"Test",
                      "birthday":"2004-06-18",
                      "gender":"M",
                      "address":"3 Club Road",
                      "phoneNumber":"300-444-5555"
                    },
                    {
                      "id":4,
                      "firstName":"TestEarlyOnset",
                      "lastName":"Test",
                      "birthday":"2002-06-28",
                      "gender":"F",
                      "address":"4 Valley Dr",
                      "phoneNumber":"400-555-6666"
                    }
                  ],
                  "pageable":{
                    "pageNumber":0,
                    "pageSize":20,
                    "sort":{
                      "sorted":false,
                      "empty":true,
                      "unsorted":true
                    },
                    "offset":0,
                    "paged":true,
                    "unpaged":false
                  },
                  "last":true,
                  "totalElements":4,
                  "totalPages":1,
                  "size":20,
                  "number":0,
                  "sort":{
                    "sorted":false,
                    "empty":true,
                    "unsorted":true
                  },
                  "first":true,
                  "numberOfElements":4,
                  "empty":false
                }
                """, mvcResult.getResponse().getContentAsString(), true);
    }

    @Test
    @WithMockUser(username = "user1", password = "password1")
    void shouldReturnAllPatientsForSecondPage() throws Exception {
        // Given
        // When
        MvcResult mvcResult = mockMvc.perform(getRequest(1, 2))
                .andExpect(status().isOk())
                .andReturn();
        // Then
        JSONAssert.assertEquals("""
                {
                  "content":[
                    {
                      "id":3,
                      "firstName":"TestInDanger",
                      "lastName":"Test",
                      "birthday":"2004-06-18",
                      "gender":"M",
                      "address":"3 Club Road",
                      "phoneNumber":"300-444-5555"
                    },
                    {
                      "id":4,
                      "firstName":"TestEarlyOnset",
                      "lastName":"Test",
                      "birthday":"2002-06-28",
                      "gender":"F",
                      "address":"4 Valley Dr",
                      "phoneNumber":"400-555-6666"
                    }
                  ],
                  "pageable":{
                    "pageNumber":1,
                    "pageSize":2,
                    "sort":{
                      "sorted":false,
                      "empty":true,
                      "unsorted":true
                    },
                    "offset":2,
                    "paged":true,
                    "unpaged":false
                  },
                  "last":true,
                  "totalElements":4,
                  "totalPages":2,
                  "first":false,
                  "size":2,
                  "number":1,
                  "sort":{
                    "sorted":false,
                    "empty":true,
                    "unsorted":true
                  },
                  "numberOfElements":2,
                  "empty":false
                }
                """, mvcResult.getResponse().getContentAsString(), true);
    }
}

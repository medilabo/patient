package com.openclassrooms.medilabo.patient.IT.controller;

import com.openclassrooms.medilabo.patient.model.GenderEnum;
import com.openclassrooms.medilabo.patient.model.Patient;
import com.openclassrooms.medilabo.patient.repository.PatientRepository;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class PutPatientIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    PatientRepository patientRepository;

    private MockHttpServletRequestBuilder getRequest(Long id) {
        return put("/patients/" + id)
                .contentType(MediaType.APPLICATION_JSON);
    }

    @Test
    void shouldReturn401WhenUnAuthenticated() throws Exception {
        // Given
        // When
        mockMvc.perform(getRequest(1L))
                .andExpect(status().isUnauthorized());
        // Then
    }

    @Test
    @WithMockUser(username = "user1", password = "password1")
    void shouldPutPatient() throws Exception {
        // Given
        // When
        MvcResult mvcResult = mockMvc.perform(getRequest(1L)
                        .content("""
                                {
                                  "firstName": "NewFirstname",
                                  "lastName": "NewLastname",
                                  "gender": "M",
                                  "birthday": "1968-12-31",
                                  "address": "New Address",
                                    "phoneNumber": "0612345678"
                                }
                                  """))
                .andExpect(status().isOk())
                .andReturn();
        // Then
        Patient patient = patientRepository.findById(1L).orElseThrow();
        Patient expectedPatient = Patient.builder()
                .id(1L)
                .firstName("NewFirstname")
                .lastName("NewLastname")
                .gender(GenderEnum.M)
                .birthday(LocalDate.of(1968, 12, 31))
                .address("New Address")
                .phoneNumber("0612345678")
                .build();
        assertThat(patient).usingRecursiveComparison().isEqualTo(expectedPatient);

        JSONAssert.assertEquals("""
                {
                  "id":1,
                  "firstName":"NewFirstname",
                  "lastName":"NewLastname",
                  "gender":"M",
                  "birthday":"1968-12-31",
                  "address":"New Address",
                    "phoneNumber":"0612345678"
                    }
                    """, mvcResult.getResponse().getContentAsString(), true);
    }

    @Test
    @WithMockUser(username = "user1", password = "password1")
    void shouldReturn404WhenNoSuchPatient() throws Exception {
        // Given
        // When
        MvcResult result = mockMvc.perform(getRequest(999L)
                        .content("""
                                {
                                  "firstName": "NewFirstname",
                                  "lastName": "NewLastname",
                                  "gender": "M",
                                    "birthday": "1968-12-31"
                                    }
                                    """))
                .andExpect(status().isNotFound())
                .andReturn();
        // Then
        JSONAssert.assertEquals("""
                {
                  "error":"NO_SUCH_PATIENT",
                  "id":999
                }
                """, result.getResponse().getContentAsString(), true);
    }

    @Test
    @WithMockUser(username = "user1", password = "password1")
    void shouldReturn400WhenInvalidGender() throws Exception {
        // Given
        // When
        MvcResult result = mockMvc.perform(getRequest(1L)
                        .content("""
                                {
                                  "firstName": "New Firstname",
                                  "lastName": "New Lastname",
                                  "gender": "X",
                                    "birthday": "1968-12-31"
                                    }
                                    """))
                .andExpect(status().isBadRequest())
                .andReturn();
        // Then
        JSONAssert.assertEquals("""
                {
                  "error":"INVALID_PARAMETERS",
                  "details":{
                    "gender":{
                      "field":"gender",
                      "constraint":"InvalidFormat",
                      "message":"must be a value of [M, F]"
                    }
                  }
                }
                """, result.getResponse().getContentAsString(), true);
    }

    @Test
    @WithMockUser(username = "user1", password = "password1")
    void shouldReturn400WhenEmptyBody() throws Exception {
        // Given
        // When
        MvcResult result = mockMvc.perform(getRequest(1L)
                        .content("""
                                {
                                }
                                    """))
                .andExpect(status().isBadRequest())
                .andReturn();
        // Then
        JSONAssert.assertEquals("""
                {
                  "error":"INVALID_PARAMETERS",
                  "details":{
                    "birthday":{
                      "field":"birthday",
                      "constraint":"NotNull",
                      "message":"must not be null"
                    },
                    "firstName":{
                      "field":"firstName",
                      "constraint":"NotBlank",
                      "message":"must not be blank"
                    },
                    "lastName":{
                      "field":"lastName",
                      "constraint":"NotBlank",
                      "message":"must not be blank"
                    },
                    "gender":{
                      "field":"gender",
                      "constraint":"NotNull",
                      "message":"must not be null"
                    }
                  }
                }
                """, result.getResponse().getContentAsString(), true);
    }

    @Test
    @WithMockUser(username = "user1", password = "password1")
    void shouldReturn400WhenBadPhoneFirstnameAndLastname() throws Exception {
        // Given
        // When
        MvcResult result = mockMvc.perform(getRequest(1L)
                        .content("""
                                {
                                    "birthday": "1968-12-31",
                                    "gender": "M",
                                    "firstName": "New Firstname2",
                                    "lastName": "New Lastname2",
                                    "phoneNumber": "1234567890"
                                }
                                    """))
                .andExpect(status().isBadRequest())
                .andReturn();
        // Then
        JSONAssert.assertEquals("""
                {
                   "details":{
                     "firstName":{
                       "field":"firstName",
                       "constraint":"Pattern",
                       "message":"must contain only letters"
                     },
                     "lastName":{
                       "field":"lastName",
                       "constraint":"Pattern",
                       "message":"must contain only letters"
                     },
                     "phoneNumber":{
                       "field":"phoneNumber",
                       "constraint":"Pattern",
                       "message":"must start with 0 and contain 10 digits"
                     }
                   },
                   "error":"INVALID_PARAMETERS"
                 }
                """, result.getResponse().getContentAsString(), true);
    }
}

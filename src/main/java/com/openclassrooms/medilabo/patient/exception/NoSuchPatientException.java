package com.openclassrooms.medilabo.patient.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class NoSuchPatientException extends RuntimeException {
    private final Long id;
}
package com.openclassrooms.medilabo.patient.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@ControllerAdvice
@Slf4j
public class AppExceptionHandler {

    @ExceptionHandler(NoSuchPatientException.class)
    public ResponseEntity<Map<String, Object>> handleNoSuchPatientException(NoSuchPatientException e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                Map.of(
                        "error", "NO_SUCH_PATIENT",
                        "id", e.getId()
                )
        );
    }

    @ExceptionHandler(ValueOfEnumException.class)
    public ResponseEntity<Map<String, Object>> handleMethodArgumentTypeMismatchException(ValueOfEnumException e) {
        String message = "must be a value of " + Arrays.toString(e.getEnumType().getEnumConstants());
        String field = e.getField();
        Map<String, Object> result = new HashMap<>();
        result.put(field, new RejectedField(field, "InvalidFormat", message));
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                Map.of(
                        "error", "INVALID_PARAMETERS",
                        "details", result)
        );
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Map<String, Object>> handleInvalidParameters(MethodArgumentNotValidException e) {
        Map<String, Object> result = new HashMap<>();
        List<FieldError> errors = e.getFieldErrors();
        errors.forEach(err -> {
            String field = err.getField();
            result.put(field, new RejectedField(field, err.getCode(), err.getDefaultMessage()));
        });
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                Map.of(
                    "error", "INVALID_PARAMETERS",
                        "details", result)
        );
    }

    record RejectedField(String field, String constraint, String message) {
    }
}
package com.openclassrooms.medilabo.patient.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Getter
@RequiredArgsConstructor
public class ValueOfEnumException extends RuntimeException {
    private final String field;
    private final String value;
    private final Class<? extends Enum<?>> enumType;
}
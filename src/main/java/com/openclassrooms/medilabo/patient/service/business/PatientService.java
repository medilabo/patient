package com.openclassrooms.medilabo.patient.service.business;

import com.openclassrooms.medilabo.patient.exception.NoSuchPatientException;
import com.openclassrooms.medilabo.patient.mapper.PatientMapper;
import com.openclassrooms.medilabo.patient.model.Patient;
import com.openclassrooms.medilabo.patient.model.dto.request.PatientRequestDto;
import com.openclassrooms.medilabo.patient.model.dto.response.PatientResponseDto;
import com.openclassrooms.medilabo.patient.repository.PatientRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class PatientService {

    private final PatientRepository patientRepository;
    private final PatientMapper patientMapper;

    public Page<PatientResponseDto> getPatients(Pageable pageable) {
        Page<Patient> patients = patientRepository.findAllByOrderByIdAsc(pageable);
        return patients.map(patient -> patientMapper.toResponseDto(patient));
    }

    public PatientResponseDto patchPatient(Long id, PatientRequestDto dto) {
        Patient patient = getPatientById(id);
        patientMapper.putPatient(patient, dto);
        return patientMapper.toResponseDto(patient);
    }

    public PatientResponseDto createPatient(PatientRequestDto dto) {
        Patient patient = patientMapper.toEntity(dto);
        patient = patientRepository.save(patient);
        return patientMapper.toResponseDto(patient);
    }

    public PatientResponseDto getPatient(Long id) {
        Patient patient = getPatientById(id);
        return patientMapper.toResponseDto(patient);
    }

    private Patient getPatientById(Long id) {
        return patientRepository.findById(id)
                .orElseThrow(() -> new NoSuchPatientException(id));
    }
}

package com.openclassrooms.medilabo.patient.model.dto.request;

import com.openclassrooms.medilabo.patient.model.GenderEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;

import java.time.LocalDate;

public record PatientRequestDto(
        @NotBlank
        @Pattern(regexp = "$|^[A-Za-z]+$", message = "must contain only letters")
        String firstName,
        @NotBlank
        @Pattern(regexp = "$|^[A-Za-z]+$", message = "must contain only letters")
        String lastName,
        @NotNull
        LocalDate birthday,
        @NotNull
        @Schema(type = "string")
        GenderEnum gender,
        String address,
        @Pattern(regexp = "^$|^0[0-9]{9}$", message = "must start with 0 and contain 10 digits")
        String phoneNumber
) {
}

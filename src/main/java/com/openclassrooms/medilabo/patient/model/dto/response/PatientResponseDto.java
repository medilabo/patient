package com.openclassrooms.medilabo.patient.model.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;

import java.time.LocalDate;

@Schema(name = "PatientResponse")
public record PatientResponseDto(
        Long id,
        String firstName,
        String lastName,
        LocalDate birthday,
        String gender,
        String address,
        String phoneNumber
) {
}

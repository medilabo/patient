package com.openclassrooms.medilabo.patient.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.openclassrooms.medilabo.patient.exception.ValueOfEnumException;

public enum GenderEnum {
    M,
    F;

    @JsonValue
    public String getValue() {
        return this.name();
    }

    @JsonCreator
    public static GenderEnum forValue(String value) {
        for (GenderEnum gender : GenderEnum.values()) {
            if (gender.name().equalsIgnoreCase(value)) {
                return gender;
            }
        }
        throw new ValueOfEnumException("gender", value, GenderEnum.class);
    }
}

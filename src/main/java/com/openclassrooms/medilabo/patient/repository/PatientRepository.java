package com.openclassrooms.medilabo.patient.repository;

import com.openclassrooms.medilabo.patient.model.Patient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientRepository extends JpaRepository<Patient, Long> {
    Page<Patient> findAllByOrderByIdAsc(Pageable pageable);
}

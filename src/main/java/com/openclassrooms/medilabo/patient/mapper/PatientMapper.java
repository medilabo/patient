package com.openclassrooms.medilabo.patient.mapper;

import com.openclassrooms.medilabo.patient.model.Patient;
import com.openclassrooms.medilabo.patient.model.dto.request.PatientRequestDto;
import com.openclassrooms.medilabo.patient.model.dto.response.PatientResponseDto;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper
public interface PatientMapper {

    PatientResponseDto toResponseDto(Patient patient);

    void putPatient(@MappingTarget Patient patient, PatientRequestDto dto);

    Patient toEntity(PatientRequestDto dto);
}

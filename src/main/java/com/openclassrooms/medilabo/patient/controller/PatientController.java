package com.openclassrooms.medilabo.patient.controller;

import com.openclassrooms.medilabo.patient.model.Patient;
import com.openclassrooms.medilabo.patient.model.dto.request.PatientRequestDto;
import com.openclassrooms.medilabo.patient.model.dto.response.PatientResponseDto;
import com.openclassrooms.medilabo.patient.service.business.PatientService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@SecurityRequirement(name = "bearerAuth")
@Tag(name = "Patient")
public class PatientController {

    private final PatientService patientService;

    @GetMapping("/patients")
    @Operation(operationId = "getPatients")
    public ResponseEntity<Page<PatientResponseDto>> getPatients(
            @PageableDefault(size = 20) Pageable pageable
    ) {
        Page<PatientResponseDto> patients = patientService.getPatients(pageable);
        return ResponseEntity.ok(patients);
    }

    @PutMapping(value = "/patients/{id}", consumes = "application/json", produces = "application/json")
    @Operation(operationId = "updatePatient")
    public ResponseEntity<PatientResponseDto> updatePatient(
            @PathVariable Long id,
            @RequestBody @Valid PatientRequestDto dto
    ) {
        PatientResponseDto updatedPatient = patientService.patchPatient(id, dto);
        return ResponseEntity.ok(updatedPatient);
    }

    @PostMapping(value = "/patients", consumes = "application/json", produces = "application/json")
    @Operation(operationId = "createPatient")
    public ResponseEntity<PatientResponseDto> createPatient(
            @RequestBody @Valid PatientRequestDto dto
    ) {
        PatientResponseDto createdPatient = patientService.createPatient(dto);
        return ResponseEntity.ok(createdPatient);
    }

    @GetMapping("/patients/{id}")
    @Operation(operationId = "getPatient")
    public ResponseEntity<PatientResponseDto> getPatient(
            @PathVariable Long id
    ) {
        PatientResponseDto patient = patientService.getPatient(id);
        return ResponseEntity.ok(patient);
    }
}

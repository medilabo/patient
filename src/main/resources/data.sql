INSERT INTO patient (id, first_name, last_name, birthday, gender, address, phone_number) VALUES
                                                                                             (1, 'TestNone', 'Test', '1966-12-31', 'F', '1 Brookside St', '100-222-3333'),
                                                                                             (2, 'TestBorderline', 'Test', '1945-06-24', 'M', '2 High St', '200-333-4444'),
                                                                                             (3, 'TestInDanger', 'Test', '2004-06-18', 'M', '3 Club Road', '300-444-5555'),
                                                                                             (4, 'TestEarlyOnset', 'Test', '2002-06-28', 'F', '4 Valley Dr', '400-555-6666')
ON CONFLICT (id) DO NOTHING;
SELECT setval('patient_id_seq', (SELECT MAX(id) FROM patient));
